Tornado Shell Commands Runner with WebSockets
=============================================

This is an application for shell commands running.
The application is based on `Tornado <http://www.tornadoweb.org>`_ (a Python web framework and
asynchronous networking library) and `WebSockets <http://en.wikipedia.org/wiki/WebSocket>`_
with common and Google OAuth2 authentication. By using non-blocking network I/O, Tornado
can scale to tens of thousands of open connections, making it ideal for
applications that require a long-lived connection to each user.


Getting up and running
----------------------

NOTE: Requires `virtualenv,`_ virtualenvwrapper_.

.. _`virtualenv,`: http://virtualenv.readthedocs.org/en/latest/
.. _virtualenvwrapper: http://virtualenvwrapper.readthedocs.org/en/latest/

The steps below will get you up and running with a local development environment. It is assumed you have the following installed:

* pip
* virtualenv

First make sure to create and activate a virtualenv_::

    $ mkvirtualenv --python=/usr/bin/python3 commandws

Then open a terminal at the project root and install the requirements for local running::

    $ pip install -r requirements.txt

.. _virtualenv: http://docs.python-guide.org/en/latest/dev/virtualenvs/

One can now run the application::

    $ python main.py

The application start. One can access it at http://localhost:5000.


Setting up
----------

It is possible to set up a timeout for long running processes (default 15 sec) changing the timeout attribute
of the CommSocketHandler class. Also one can change max_workers quantity (default 4) for the thread_pool attribute
of the CommSocketHandler class.

Deployment
----------

It is strongly not recommended to deploy the application without authentication-authorization system.
It's possible to deploy to Heroku if you know what are you doing.

Heroku
^^^^^^

Run these commands to deploy the project to Heroku:

.. code-block:: bash

    heroku create --buildpack https://github.com/heroku/heroku-buildpack-python

    heroku config:set cookie_secret=YOUR_COOKIE_SECRET_HERE

    heroku config:set google_oauth_key=YOUR_GOOGLE_OAUTH_KEY_HERE
    heroku config:set google_oauth_secret=YOUR_GOOGLE_SECRET_HERE

    git push heroku master
    heroku open

That's all.
