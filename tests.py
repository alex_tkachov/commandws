#!/usr/bin/env python
"""
The application tests module.
python -m tornado.testing discover
"""

import datetime
import json
import mock
from tornado.testing import AsyncHTTPTestCase
from tornado import testing, httpserver, websocket, gen
from main import Application, BaseHandler


app = Application()


class TestHandlerBase(AsyncHTTPTestCase):

    def setUp(self):
        super(TestHandlerBase, self).setUp()
        server = httpserver.HTTPServer(app)
        socket, self.port = testing.bind_unused_port()
        server.add_socket(socket)

    def get_app(self):
        self.app = app
        return self.app

    def _mk_connection(self):
        return websocket.websocket_connect(
            'ws://localhost:{}/commandsocket'.format(self.port)
        )

    # # this test is run in test_run_command
    # @testing.gen_test
    # def test_websocket(self):
    #     c = yield self._mk_connection()
    #     response = yield c.read_message()
    #     self.assertIn('Users connected:', response)

    @gen.coroutine
    def _mk_client(self):
        c = yield self._mk_connection()

        # Discard the first message
        # This could be any initial handshake, which needs to be generalized
        # for most of the tests.
        _ = yield c.read_message()
        self.assertIn('Users connected:', _)

        raise gen.Return(c)

    @testing.gen_test
    def test_run_command(self):

        with mock.patch.object(BaseHandler, 'get_current_user') as mock_method:
            mock_method.return_value = {"name": "admin"}

            c = yield self._mk_client()

            c.write_message(json.dumps({"body": "date +'%Y-%m-%d'"}))
            now = datetime.datetime.now().strftime("%Y-%m-%d")
            response = yield c.read_message()
            self.assertIn(now, response)

    def test_get_index_page(self):
        response = self.fetch('/')
        self.assertEqual(response.code, 200)
        self.assertIn('/auth/common?next=%2F', response.effective_url)

    def test_get_common_login_page(self):
        response = self.fetch('/auth/common')
        self.assertEqual(response.code, 200)

    def test_common_login(self):
        self.app.settings['xsrf_cookies'] = False
        response = self.fetch('/auth/common',
                              method="POST",
                              body=b'name=admin&password=admin',
                              follow_redirects=False,
                              )
        self.assertEqual(response.code, 302)
        self.get_app().settings['xsrf_cookies'] = True

    # # you have to register your application with your account
    # def test_google_login_page(self):
    #     response = self.fetch('/auth/google')
    #     self.assertEqual(response.code, 200)
