#!/usr/bin/env python
"""Shell commands runner for websockets.
with Authentication, error handling, etc.
"""

import logging
import os.path
import re
import subprocess
import sys
import tornado.auth
import tornado.escape
import tornado.gen
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import uuid

from concurrent.futures.thread import ThreadPoolExecutor
from datetime import datetime
from tornado.options import define, options


define("port", default=5000, help="run on the given port", type=int)
define("debug", default=False, help="run in debug mode")


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", MainHandler),
            (r"/auth/common", CommonLoginHandler),
            (r"/auth/google", GoogleOAuth2LoginHandler),
            (r"/auth/logout", AuthLogoutHandler),
            (r"/commandsocket", CommSocketHandler),
        ]
        settings = dict(
            cookie_secret=os.getenv('cookie_secret', '__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__'),
            login_url="/auth/common",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=True,
            debug=options.debug,
            google_oauth={"key": os.getenv('google_oauth_key', 'your_google_oauth_key'),
                          "secret": os.getenv('google_oauth_secret', 'your_google_oauth_secret')},
        )
        super(Application, self).__init__(handlers, **settings)


class BaseHandler(tornado.web.RequestHandler):
    """
    Base application handler
    """
    def get_current_user(self):
        user_json = self.get_secure_cookie("admin_user")
        if not user_json: return None
        return tornado.escape.json_decode(user_json)


class MainHandler(BaseHandler):
    """
    Main application handler
    """
    @tornado.web.authenticated
    def get(self):
        self.render("index.html", messages=CommSocketHandler.cache, userscount=CommSocketHandler.userscount)


class CommSocketHandler(tornado.websocket.WebSocketHandler, BaseHandler):
    """
    WebSocket handler. BaseHandler inheritance allows to use methods such as get_current_user etc.
    """
    waiters = set()
    cache = []
    cache_size = 200
    userscount = {"users": ''}

    thread_pool = ThreadPoolExecutor(4)
    timeout = 15

    def get_compression_options(self):
        # Non-None enables compression with default options.
        return {}

    def get_users_count(self):
        userscount = {
            "users": str(len(self.waiters))
        }
        userscount["html"] = tornado.escape.to_basestring(
            self.render_string("userscount.html", userscount=userscount))

        CommSocketHandler.update_users(userscount)
        CommSocketHandler.send_updates(userscount)

    def open(self):
        CommSocketHandler.waiters.add(self)
        self.get_users_count()

    def on_close(self):
        CommSocketHandler.waiters.remove(self)
        self.get_users_count()

    @classmethod
    def update_users(cls, mess):
        cls.userscount.update(mess)

    @classmethod
    def update_cache(cls, chat):
        cls.cache.append(chat)
        if len(cls.cache) > cls.cache_size:
            cls.cache = cls.cache[-cls.cache_size:]

    @classmethod
    def send_updates(cls, chat):
        logging.info("sending message to %d waiters", len(cls.waiters))
        for waiter in cls.waiters:
            try:
                waiter.write_message(chat)
            except Exception:
                logging.error("Error sending message", exc_info=True)

    @tornado.web.authenticated
    @tornado.gen.coroutine
    def on_message(self, message, *args):
        # eliminates blocking properties of subprocess.Popen in self.run_command
        yield self.thread_pool.submit(self.run_command, message, *args)

    def run_command(self, message, *args):
        logging.info("got message %r", message)

        parsed = tornado.escape.json_decode(message)
        args = parsed['body']
        encoding = sys.stdout.encoding
        command_was_run = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

        proc = subprocess.Popen(
            args,
            stderr=subprocess.STDOUT,
            stdout=subprocess.PIPE,
            shell=True
        )

        try:
            outs, errs = proc.communicate(timeout=self.timeout)
        except Exception as e:
            proc.kill()
            outs, errs = (None, e)
        command = {
            "id": str(uuid.uuid4()),
            "body": parsed["body"],
            "from": self.current_user["name"],
            "now": command_was_run,
            "response": outs.decode(encoding=encoding).split("\n") if outs else [str(errs)]
            }
        command["html"] = tornado.escape.to_basestring(
            self.render_string("message.html", message=command))

        CommSocketHandler.update_cache(command)
        CommSocketHandler.send_updates(command)


class GoogleOAuth2LoginHandler(tornado.web.RequestHandler,
                               tornado.auth.GoogleOAuth2Mixin):
    """
    Google OAuth2 login handler
    """
    @tornado.gen.coroutine
    def get(self):
        if self.get_argument('code', False):
            access = yield self.get_authenticated_user(
                    redirect_uri='http://your/host/auth/google',
                    code=self.get_argument('code'))
            user = yield self.oauth2_request(
                "https://www.googleapis.com/oauth2/v1/userinfo",
                access_token=access["access_token"])
            self.set_secure_cookie("admin_user",
                                   tornado.escape.json_encode(user))
            self.redirect("/")
            return
        else:
            yield self.authorize_redirect(
                redirect_uri='http://your/host/auth/google',
                client_id=self.settings['google_oauth']['key'],
                scope=['profile', 'email'],
                response_type='code',
                extra_params={'approval_prompt': 'auto'})


class CommonLoginHandler(BaseHandler):
    """
    Common login handler
    """

    def get(self):
        if not self.current_user:
            self.render("login.html", error=None)
        else:
            self.redirect("/")

    @tornado.gen.coroutine
    def post(self):
        user = {
            'name': self.get_argument("name"),
        }

        # db credentials request emulation
        cr_name = re.compile("^admin([0-9]+)*$")        # write your login name here
        cr_password = re.compile("^admin([0-9]+)*$")    # write your password here

        if not cr_name.match(user["name"]):
            self.render("login.html", error="name not found")
            return
        if cr_password.match(self.get_argument("password")):
            remember_me = self.get_argument("remember_me", default=False)
            expires_days = 365 if remember_me else 1
            self.set_secure_cookie("admin_user", tornado.escape.json_encode(user), expires_days=expires_days)
            self.redirect(self.get_argument("next", "/"))
        else:
            self.render("login.html", error="incorrect password")


class AuthLogoutHandler(BaseHandler):
    """
     Logout handler
    """
    def get(self):
        self.clear_cookie("admin_user")
        self.redirect("/")


def main():
    try:
        tornado.options.parse_command_line()
        app = Application()
        app.listen(options.port)
        tornado.ioloop.IOLoop.current().start()
    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.current().stop()


if __name__ == "__main__":
    main()
