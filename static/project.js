$(document).ready(function() {
    if (!window.console) window.console = {};
    if (!window.console.log) window.console.log = function() {};

    var form = $('#messageform');
    var userscount = $('#userscount');
    var container = $('#inbox');

    if (container.length) {
       container[0].scrollTop = container[0].scrollHeight;
    }

    setScroll(container);

    $(window).on('resize',function() {
        setScroll(container);
    });


    container.bind("DOMSubtreeModified",function(){
        setScroll(container);
    });


    form.on("submit", "", function(e) {
        newMessage($(this));
        return false;
    });

    $("#message").select();
    updater.start();
});


function newMessage(form) {
    var message = form.formToDict();
    updater.socket.send(JSON.stringify(message));
    form.find("input[type=text]").val("").select();
}

function setScroll(selector) {
    var ratio =  Number((selector.height()/$(window).height()).toFixed(1));
    if (ratio >=0.7) {
        selector.css('height','70vh');
        selector.css('overflow-y','scroll');
        selector.animate({ scrollTop: selector[0].scrollHeight }, "slow");
    }
}

jQuery.fn.formToDict = function() {
    var fields = this.serializeArray();
    var json = {};
    for (var i = 0; i < fields.length; i++) {
        json[fields[i].name] = fields[i].value;
    }
    if (json.next) delete json.next;
    return json;
};

var updater = {
    socket: null,

    start: function() {
        var url = "ws://" + location.host + "/commandsocket";
        updater.socket = new WebSocket(url);
        updater.socket.onmessage = function(event) {
            if (event.data.indexOf('body') > -1) {
                updater.showMessage(JSON.parse(event.data));
            }
            else {
                updater.showUsers(JSON.parse(event.data));
            }
        }
    },

    showMessage: function(message) {
        var existing = $("#m" + message.id);
        if (existing.length > 0) return;
        var node = $(message.html);
        node.hide();
        $("#inbox").append(node);
        node.slideDown();
    },

    showUsers: function(message) {
        $("#users").html(message.html);

    }

};
